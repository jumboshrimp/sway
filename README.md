# jumboshrimp's Sway
Installs sway on Debian 12.
- Switches to testing/trixie
- Installs sway, swaybg and foot terminal and builds accompanying software from source, including tofi, waybar, swaylock-effects, slurp and grim
- Installs other additional software such as xplr, mpv, neovim and flatpak
- Uses zsh along with zsh-autopcomplete

# To Do
- [ ] Fix centering of text in swaylock-effects

