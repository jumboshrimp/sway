#!/bin/bash
set -euo pipefail

# Variables
SOURCES_LIST_BACKUP="/etc/apt/sources.list.bak"
SOURCES_LIST="/etc/apt/sources.list"
DEBIAN_CODENAME="bookworm"
DEBIAN_TESTING_CODENAME="trixie"
FONT_ZIP_URL="https://github.com/ryanoasis/nerd-fonts/releases/latest/download/JetBrainsMono.zip"
FONT_ZIP="JetBrainsMono.zip"
FONT_DIR="$HOME/.local/share/fonts"
TEMP_DIR="$(mktemp -d)"
USER="$(logname)" # Current user

# Functions
backup_sources_list() {
    echo "Backing up $SOURCES_LIST to $SOURCES_LIST_BACKUP"
    sudo cp "$SOURCES_LIST" "$SOURCES_LIST_BACKUP"
}

switch_to_testing() {
    echo "Switching to $DEBIAN_TESTING_CODENAME"
    sudo sed -i "s/$DEBIAN_CODENAME/$DEBIAN_TESTING_CODENAME/g" "$SOURCES_LIST"
    sudo sed -i "/$DEBIAN_CODENAME-updates/d" "$SOURCES_LIST"
}

refresh_package_db() {
    echo "Refreshing package database"
    sudo apt update -y
}

upgrade_packages() {
    echo "Upgrading packages"
    sudo apt upgrade -y
}

install_packages() {
    echo "Installing packages"
    sudo apt install -y \
        git \
        curl \
        unzip \
        sway \
        swaybg \
        swayidle \
        bluez \
        libspa-0.2-bluetooth \
        pipewire-pulse \
        neovim \
        firefox-esr \
        mpv \
        htop \
        flatpak \
        pavucontrol \
        xwayland \
        thunar
}

install_font() {
  echo "Installing JetBrainsMono Nerd Font"
  wget "$FONT_ZIP_URL"
  mkdir -p "$FONT_DIR"
  unzip -q "$FONT_ZIP" -d "$FONT_DIR"
}

# Choose which software to install
install_software() {
    chmod +x ./software/*.sh
    
}

install_fish_shell() {
    echo 'deb http://download.opensuse.org/repositories/shells:/fish:/release:/3/Debian_12/ /' | sudo tee /etc/apt/sources.list.d/shells:fish:release:3.list
    curl -fsSL https://download.opensuse.org/repositories/shells:fish:release:3/Debian_12/Release.key | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/shells_fish_release_3.gpg > /dev/null
    sudo apt update
    sudo apt install fish
}

install_starship() {
    curl -sS https://starship.rs/install.sh | sh
}

set_default_shell() {
    echo "Changing default shell to fsh"
    chsh -s /bin/fsh
}

# Main script
backup_sources_list
switch_to_testing
refresh_package_db
upgrade_packages

# Install packages
install_packages

# Install FontAwesome fonts
install_font

# Install Flatpak repo
echo "Installing Flatpak repo"
flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo

# Install additiponal software

# Define the path to the 'software' folder
software_folder="software"

# Function to get the list of software items from the filenames
get_software_items() {
    local items=()
    for filename in "$software_folder"/*.sh; do
        if [ -f "$filename" ]; then
            item="${filename##*/}"
            item="${item%.sh}"
            items+=("$item")
        fi
    done
    echo "${items[@]}"
}

# Function to print the menu
print_menu() {
    local items=($@)
    echo "Select the software items you want to install:"
    for i in "${!items[@]}"; do
        echo "$((i+1)). ${items[$i]}"
    done
    echo "q. Quit"
}

# Function to install the selected items
install_selected_items() {
    local items=($@)
    for selection in "${selected_items[@]}"; do
        script_name="${items[$((selection-1))]}.sh"
        script_path="$software_folder/$script_name"
        if [ -f "$script_path" ]; then
            echo "Installing ${items[$((selection-1))]}..."
            "$script_path"
        else
            echo "Script not found for ${items[$((selection-1))]}"
        fi
    done
}

# Get the list of software items from the filenames
software_items=($(get_software_items))

# Main loop
while true; do
    print_menu "${software_items[@]}"
    read -p "Enter your selections (separated by spaces) or 'q' to quit: " input

    if [ "$input" == "q" ]; then
        break
    fi

    selected_items=($input)
    install_selected_items "${software_items[@]}"
done

# Copy config files to home folder as the user
su "$USER" -c "cp -f -R home/. ~/"

install_fish_shell
install_starship
set_default_shell

echo "Setup is complete. Please reboot."

# Clean up
rm -rf "$TEMP_DIR"