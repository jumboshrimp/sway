# needed for history and autosuggestions
export HISTFILE=~/.zhistory
export HISTSIZE=10000
export SAVEHIST=10000

# set nveovim as default editor
export EDITOR='nvim'

# tell nnn to open new foot window for empty files
#export VISUAL=ewrap

# enable autosuggestions
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh

# make midnight commander colorful
export TERM=xterm-256color

# by default backspace deletes whole word
bindkey "^H" backward-delete-char

# up/down should search in history not step
bindkey '\e[A' history-beginning-search-backward
bindkey '\e[B' history-beginning-search-forward

# colored prompt
autoload -U colors && colors
PS1="%{%F{34}%}%n%{%F{40}%}@%{%F{46}%}%m %{%F{154}%}%1 %~ %{%f%}$ "

# called when executing a command
function preexec {
    print -Pn "\e]0;${(q)1}\e\\"
}

# startup sway on login on terminal 1
if [ "$(tty)" = "/dev/tty1" ] || [ "$(tty)" = "/dev/ttyv0" ] ; then
    if [ -z "$XDG_RUNTIME_DIR" ]; then
	export XDG_RUNTIME_DIR="$HOME/.config/xdg"
	rm -rf $XDG_RUNTIME_DIR
	mkdir -p $XDG_RUNTIME_DIR
    fi
    
    ## Remove the below if not running a VM
    export WLR_NO_HARDWARE_CURSORS=1

    export QT_QPA_PLATFORMTHEME=gtk2
    export QT_QPA_PLATFORM=wayland
    export GDK_BACKEND=wayland
    export XDG_SESSION_TYPE=wayland
    export XDG_CURRENT_DESKTOP=sway
    XDG_CURRENT_DESKTOP=sway dbus-run-session sway
    #exec sway
fi

# aliases
alias ls='ls --color=auto'
alias vi='nvim'
alias vim-='nvim'

# load .profile
source .profile
