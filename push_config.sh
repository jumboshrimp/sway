#!/bin/bash

# Remove current config from local environment
echo "Deleting local environment config"
rm -rf ~/.zshrc ~/.config

# Push config from repo to local environment
echo "Pushing config to local environment"
cp -f -R home/. ~/

# Reload Sway
sway reload