#!/bin/bash

echo "Installing slurp"

# Clone repo
git clone https://github.com/emersion/slurp

# Install dependencies
sudo apt install -y \
    meson \
    libwayland-dev \
    libcairo2-dev \
    libxkbcommon-dev \
    scdoc

# Build and install
cd slurp
meson setup build
sudo ninja -C build install
cd ..