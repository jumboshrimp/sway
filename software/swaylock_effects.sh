#!/bin/bash

echo "Installing swaylock-effects"

# Clone repo
git clone https://github.com/mortie/swaylock-effects

# Install dependencies
sudo apt install -y \
    meson \
    libwayland-dev \
    wayland-protocols \
    libxkbcommon-dev \
    libcairo2-dev \
    libgdk-pixbuf2.0-dev \
    libpam-dev \
    scdoc \
    git

# Build and install
cd swaylock-effects
meson build
sudo ninja -C build install
cd ..

## if installing PAM above doesn't work, run the following
# sudo chmod a+s /usr/local/bin/swaylock