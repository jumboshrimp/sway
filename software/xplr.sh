#!/bin/bash

echo "Installing xplr"

# Clone repo
git clone https://github.com/sayanarijit/xplr.git

# Install dependencies
sudo apt install -y \
    gcc \
    make

# Build and install
cd xplr
cargo build --locked --release --bin xplr
sudo cp target/release/xplr /usr/local/bin/
cd ..