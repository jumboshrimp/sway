#!/bin/bash

echo "Installing grim"

# Clone repo
git clone https://git.sr.ht/~emersion/grim

# Install dependencies
sudo apt install -y \
    meson \
    libwayland-dev \
    libpixman-1-dev \
    libpng-dev \
    libjpeg-dev

# Build and install
cd grim
meson setup build
sudo ninja -C build install
cd ..