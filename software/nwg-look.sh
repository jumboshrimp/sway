#!/bin/bash

echo "Installing nwg-look"

# Clone repo
git clone https://github.com/nwg-piotr/nwg-look

# Install dependencies
sudo apt install -y \
    meson \
    libwayland-dev \
    libpixman-1-dev \
    libpng-dev \
    libjpeg-dev

# Build and install
cd grim
meson setup build
sudo ninja -C build install
cd ..