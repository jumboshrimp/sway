#!/bin/bash

echo "Installing tofi"

# Clone repo
git clone https://github.com/philj56/tofi

# Install dependencies
sudo apt install -y \
    libfreetype-dev \
    libcairo2-dev \
    libpango1.0-dev \
    libwayland-dev \
    libxkbcommon-dev \
    libharfbuzz-dev \
    meson \
    scdoc \
    wayland-protocols

# Build
cd tofi
meson build
sudo ninja -C build install
cd ..