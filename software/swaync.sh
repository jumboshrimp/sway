#!/bin/bash

echo "Installing swaync"

# Clone repo
git clone https://github.com/ErikReider/SwayNotificationCenter

# Install dependencies
sudo apt install -y \
    valac \
    sassc \
    libjson-glib-dev \
    libhandy-1-dev \
    libgranite-dev \
    libgtk-layer-shell-dev \
    libnotify-bin

# Install Rust
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y
source "$HOME/.cargo/env"

# Build and install
cd SwayNotificationCenter
meson setup build --prefix=/usr
ninja -C build
sudo meson install -C build
cd ..