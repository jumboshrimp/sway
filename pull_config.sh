#!/bin/bash

# Remove config from repo
rm -rf home/*

# Pull config from local environment
cp -R ~/.zshrc ~/.config home/

# Reload Sway
sway reload
